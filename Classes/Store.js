const fs = require('fs');
const util = require('util');

class Store {
	constructor(){
        this.storeMap = new Map();
        //TODO: add fileName prop
	}
	
	add(params) {
		var data = params[1] + ':' + params[2] + '\n';
        //JSON.stringify(data);
        fs.appendFileSync('./dictionaryFile.txt', data, 'utf-8');
        return console.log('Added item...');
	}

	list() {
		fs.readFile('./dictionaryFile.txt', (error, data) => {
            console.log(data.toString());
		});
		
	}

	get(params) {
        let key = params[1];
		fs.readFile('./dictionaryFile.txt', (error, data) => {
            //this.storeMap.set(data);
            if(data.indexOf(key) >= 0){
                let s = data.indexOf(key); //key
                let m = data.indexOf(58, s); //delimeter
                let e = data.indexOf('\n', s); //value
                let val = data.slice(m + 1,e);
                console.log(val.toString());
            }
		});
	}

}

module.exports = Store;